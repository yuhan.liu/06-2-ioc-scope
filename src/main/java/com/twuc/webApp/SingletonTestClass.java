package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonTestClass {

    private SingletonTestHistory singletonTestHistory;

    public SingletonTestClass(SingletonTestHistory singletonTestHistory) {
        this.singletonTestHistory = singletonTestHistory;
        this.singletonTestHistory.addHistory("Class SingletonTestClass created.");
    }

    public SingletonTestHistory getSingletonTestHistory() {
        singletonTestHistory.addHistory("SingletonTestClass - getSingletonTestHistory called.");
        return singletonTestHistory;
    }
}

