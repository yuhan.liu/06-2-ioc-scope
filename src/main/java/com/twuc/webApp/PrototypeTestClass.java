package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeTestClass {

    private PrototypeTestHistory prototypeTestHistory;

    public PrototypeTestClass(PrototypeTestHistory prototypeTestHistory) {
        this.prototypeTestHistory = prototypeTestHistory;
        this.prototypeTestHistory.addLog("Class PrototypeTestClass created.");
    }

    public PrototypeTestHistory getPrototypeTestHistory() {
        prototypeTestHistory.addLog("PrototypeTestClass - getPrototypeTestHistory called.");
        return prototypeTestHistory;
    }
}