package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PrototypeTestHistory {

    private List<String> history = new ArrayList<>();

    public PrototypeTestHistory() {
        history.add("Class PrototypeTestHistory created.");
    }

    public List<String> getHistory() {
        return history;
    }

    void addLog(String record) {
        history.add(record);
    }
}

