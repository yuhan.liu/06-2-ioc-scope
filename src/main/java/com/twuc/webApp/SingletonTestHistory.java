package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SingletonTestHistory {

    private List<String> history = new ArrayList<>();

    public SingletonTestHistory() {
        history.add("Class SingletonTestHistory created.");
    }

    public List<String> getHistory() {
        return history;
    }

    void addHistory(String record) {
        history.add(record);
    }
}
