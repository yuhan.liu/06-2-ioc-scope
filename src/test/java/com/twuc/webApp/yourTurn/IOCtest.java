package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IOCtest {

    private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

    //2.1
    @Test
    void should_return_result_with_compare() {
        InterfaceOneImpl bean = context.getBean(InterfaceOneImpl.class);
        InterfaceOne bean2 = context.getBean(InterfaceOne.class);
        assertEquals(bean, bean2);
    }

    @Test
    void should_return_result_class_and_his_kid() {
        InterfaceOneImpl bean = context.getBean(InterfaceOneImpl.class);
        InterfaceOneImplKid bean2 = context.getBean(InterfaceOneImplKid.class);
        assertEquals(bean, bean2);
    }

    @Test
    void should_return_result_class_with_abstract_class() {
        DerivedClass bean = context.getBean(DerivedClass.class);
        AbstractBaseClass bean2 = context.getBean(AbstractBaseClass.class);
        assertEquals(bean, bean2);
    }

    @Test
    void should_return_result_class_with_prototype_scope() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(bean, bean2);
    }

    //2.2

    @Test
    void should_know_when_object_create_use_getbean() {
        SingletonTestClass singleton = context.getBean(SingletonTestClass.class);
        SingletonTestClass anotherSingleton = context.getBean(SingletonTestClass.class);
        SingletonTestHistory singletonTestHistory = singleton.getSingletonTestHistory();
        List<String> history = singletonTestHistory.getHistory();

        assertSame(SingletonTestClass.class, singleton.getClass());
        assertSame(singleton, anotherSingleton);
        assertSame(SingletonTestHistory.class, singletonTestHistory.getClass());
        assertEquals(Arrays.asList(
                "Class SingletonTestHistory created.",
                "Class SingletonTestClass created.",
                "SingletonTestClass - getSingletonTestHistory called."
        ), history);
    }

    @Test
    void should_create_prototype_object_when_get_bean() {
        PrototypeTestClass prototype = context.getBean(PrototypeTestClass.class);
        PrototypeTestClass anotherPrototype = context.getBean(PrototypeTestClass.class);
        PrototypeTestHistory prototypeTestHistory = prototype.getPrototypeTestHistory();
        List<String> history = prototypeTestHistory.getHistory();

        assertSame(PrototypeTestClass.class, prototype.getClass());
        assertNotSame(prototype, anotherPrototype);
        assertSame(PrototypeTestHistory.class, prototypeTestHistory.getClass());
        assertEquals(Arrays.asList(
                "Class PrototypeTestHistory created.",
                "Class PrototypeTestClass created.",
                "Class PrototypeTestClass created.",
                "PrototypeTestClass - getPrototypeTestHistory called."
        ), history);
    }

    //2.3

    @Test
    void should_create_one_singleton_and_different_prototype_method_call_given_singleton_depends_on_prototype_when_prototype_call_get_random() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(SingletonDependsOnPrototypeProxy.class);
        int random = singleton.getPrototypeRandom();

        SingletonDependsOnPrototypeProxy anotherSingleton = context.getBean(SingletonDependsOnPrototypeProxy.class);
        int anotherRandom = anotherSingleton.getPrototypeRandom();

        assertSame(singleton.getClass(), anotherSingleton.getClass());
        assertNotEquals(random, anotherRandom);
    }

    @Test
    void should_create_one_singleton_and_different_prototype_method_call_given_singleton_depends_on_prototype_when_prototype_call_to_string() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        String prototypeId = singleton.getPrototypeDependentWithProxy().toString();

        SingletonDependsOnPrototypeProxyBatchCall anotherSingleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        String anotherPrototypeId = anotherSingleton.getPrototypeDependentWithProxy().toString();

        assertSame(singleton.getClass(), anotherSingleton.getClass());
        assertNotEquals(prototypeId, anotherPrototypeId);
    }

    @Test
    void should_create_one_singleton_and_get_same_prototype_given_singleton_depends_on_prototype_when_get_prototype() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        PrototypeDependentWithProxy prototype = singleton.getPrototypeDependentWithProxy();

        SingletonDependsOnPrototypeProxyBatchCall anotherSingleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        PrototypeDependentWithProxy anotherPrototype = anotherSingleton.getPrototypeDependentWithProxy();

        assertSame(prototype, anotherPrototype);
        assertNotSame(PrototypeDependentWithProxy.class, prototype.getClass());
        assertTrue(prototype.getClass().getName().matches(".*EnhancerBySpringCGLIB.*"));
        assertNotSame(PrototypeDependentWithProxy.class, anotherPrototype.getClass());
        assertTrue(anotherPrototype.getClass().getName().matches(".*EnhancerBySpringCGLIB.*"));
    }
}
